with lead_created_date as (
  select
  	id lead_id , converted_opportunity_id as opp_id_c, converted_account_id  acc_id_c, l.converted_contact_id contact_id_c,
  	created_date as timestamp, null as field , 'created' as activity , 'lead_created' as source
  from
  	salesforce.lead l
)
, lead_history_data as (
  select
  	lead_id, converted_opportunity_id as opp_id_c, converted_account_id  acc_id_c, l3.converted_contact_id contact_id_c,
  	lh.created_date as timestamp, lh.field , lh.new_value , 'l_history' as source
  from salesforce."lead" l3
  	join salesforce.lead_history lh on l3.id = lh.lead_id
  where field ilike '%status%'
)
, lead_events as (
	select *
	from (
		select who_id event_lead , l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
			e.activity_date_time , concat('event_',e."type") as field , e.subject as activity, 'lead_event' as source
		from salesforce."event" e
		left join salesforce.lead l on e.who_id = l.id
		where who_id like '%00Q%'
	) lead_events
union all
	select *
	from (
		select case when l.id is null then what_id else l.id end as event_lead , l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
			e.activity_date_time , concat('event_',e."type") as field , e.subject as activity, 'opp_event' as source
		from salesforce."event" e
		left join salesforce.lead l on e.what_id = l.converted_opportunity_id
		where what_id like '%006%' and who_id is null
	) opp_events
union all
	select *
	from (
		select  case when l.id is null then what_id else l.id end as event_lead, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
			e.activity_date_time ,
			concat('event_',e."type") as field , e.subject as activity, 'acc_event' as source
		from salesforce."event" e
		left join salesforce.lead l on e.what_id = l.converted_account_id
		where what_id like '%001%' and who_id is null
	) account_events
/*union all
	select *
	from (		-- we have duplicates at contact ID level
		select case when l.id is null then who_id else l.id end as event_lead, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
			e.activity_date_time ,
			concat('event_',e."type") as field , e.subject as activity, 'contact_event' as source
		from salesforce."event" e
		left join salesforce.lead l on e.who_id = l.converted_contact_id
		where who_id like '%003%' --- and e.id = '00U5w00000eDeDrEAK' -- and l.id is not null
		order by 1
	) contact_events*/
)
, opp_created as (
select case when l.id is null then o.id else l.id end as caseid, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	o.created_date as timestamp, null as field,
	'created' as activity, 'opp_created' as source
from salesforce.opportunity o
left join salesforce."lead" l on o.id = l.converted_opportunity_id
)
, opp_history as (
select case when l.id is null then oh.opportunity_id else l.id end as caseid, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	oh.created_date as timestamp, null as field,
	stage_name as activity, 'opp_history' as source
from salesforce.opportunity_history oh
left join salesforce."lead" l on oh.opportunity_id = l.converted_opportunity_id
)
, acc_created as (
select case when l.id is null then o.id else l.id end as caseid, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	o.created_date as timestamp, null as field,
	'created' as activity, 'acc_created' as source
from salesforce.account o
left join salesforce."lead" l on o.id = l.converted_account_id
)
, acc_history as (
select case when l.id is null then ah.account_id else l.id end as caseid, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	ah.created_date as timestamp, field,
	new_value as activity, 'acc_history' as source
from salesforce.account_history ah
left join salesforce."lead" l on ah.account_id = l.converted_account_id
where field ilike '%status%'
)
,  mail_tp_outreach as (
select
	dc.id as lead_id, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	m.delivered_at as timestamp, m.subject, 'mailing' as activity, 'outreach_mail' as source
from
	outreach.mailing m
join outreach.data_connection dc on dc.parent_id = m.relationship_prospect_id
left join salesforce."lead" l on dc.id = l.id
where
	1 = 1
	and m.relationship_prospect_id is not null
	and dc.type = 'Lead'
	and dc.parent_type = 'prospect'
)
, call_tp_outreach as (
select
	dc.id as lead_id, l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
	c.completed_at as timestamp, null as subject, 'calling' as activity, 'outreach_call' as source
from
	outreach."call" c
join outreach.data_connection dc on dc.parent_id = c.relationship_prospect_id
left join salesforce."lead" l on dc.id = l.id
where
	1 = 1
	and c.relationship_prospect_id is not null
	and dc.type = 'Lead'
)
, mail_tp_hubspot as (
  select c.property_salesforceleadid , l.converted_opportunity_id , l.converted_account_id , l.converted_contact_id ,
  	ee.created email_created, ees.subject as value, type as activity, 'hubspot_email' as source
  from hubspot.email_event ee
  left join hubspot.email_event_sent ees on ee.id = ees.id
  left join hubspot.contact c on c.property_email = ee.recipient
  left join salesforce."lead" l on c.property_salesforceleadid = l.id
  where c.property_email is not null and "type" = 'SENT' and property_salesforceleadid is not null and property_salesforceleadid != ''
)
, final_table as (
select * from lead_created_date
union all
select * from lead_history_data
union all
select * from lead_events
union all
select * from opp_created
union all
select * from opp_history
union all
select * from acc_created
union all
select * from acc_history
union all
select	* from	mail_tp_outreach
union all
select	* from	call_tp_outreach
union all
select * from mail_tp_hubspot
)
select *
	-- substring(lead_id,1,3) str, count(*) cn
	-- contact_id_c, count(distinct lead_id) cn
from final_table
where 1 = 1
	-- and contact_id_c = '0035w00003TuIrJAAV' and source != 'contact_event'
	-- and contact_id_c is not null and source in ('outreach_mail')
	-- and contact_id_c = '0035w00003TuddJAAR'
	-- and source = 'hubspot_email' and opp_id_c is not null
-- group by 1
order by 1,5 
