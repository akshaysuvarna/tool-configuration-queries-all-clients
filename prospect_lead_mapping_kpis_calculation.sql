with task_data as (
  select p.id prospect_id, dc.id lead_id_pr, p.created_at prospect_created_at, m.created_at mail_time, m.id email_id, c.created_at call_time, c.id call_id
  from claroty_outreach.prospect p 
  left join claroty_outreach.data_connection dc on dc.parent_id = p.id
  left join claroty_outreach.mailing m on p.id = m.relationship_prospect_id 
  left join claroty_outreach."call" c on p.id = c.relationship_prospect_id 
  where dc."type" = 'Lead'			--- 1645 distinct leads with emails | 4871 emails 
 --  and dc.id = '00Q4H00000kFHf3UAG'
)
, tat_data as (		--- 71005 leads in total after removing deleted leads 
  select l.id lead_id , a.mql_time, b.sql_time, c.sal_time, d.aql_time, e.sdr_time
  from claroty_salesforce.lead l
  left join
  (
  	select lead_id , min(lh.created_date) mql_time 
  	from claroty_salesforce.lead_history lh 
  	where new_value = 'MQL'
  	group by 1			--- 7182 leads with mql timestamp 
  ) a on l.id = a.lead_id
  left join 
  (
  	select lead_id , min(lh.created_date) sql_time 
  	from claroty_salesforce.lead_history lh 
  	where new_value = 'SQL'
  	group by 1			--- 4338 leads with sql timestamp 
  ) b on l.id = b.lead_id
  left join 
  (
  	select lead_id , min(lh.created_date) sal_time 
  	from claroty_salesforce.lead_history lh 
  	where new_value = 'Sales Accepted'
  	group by 1			--- 374 leads with sql timestamp 
  ) c on l.id = c.lead_id
  left join 
  (
  	select lead_id , min(lh.created_date) aql_time 
  	from claroty_salesforce.lead_history lh 
  	where new_value = 'AQL'
  	group by 1			--- 11918 leads with sql timestamp 
  ) d on l.id = d.lead_id
  left join 
  (
  	select lead_id , min(lh.created_date) sdr_time 
  	from claroty_salesforce.lead_history lh 
  	where new_value = 'SDR Qualified'
  	group by 1			--- 434 leads with sql timestamp 
  ) e on l.id = e.lead_id
  where l.is_deleted = false
)
select 
	count(distinct lead_id)::numeric all_leads,
	count(distinct(case when lead_id_pr is not null then lead_id else null end))::numeric prospect_leads,
	count(distinct(case when mail_time is not null then lead_id else null end))::numeric mail_leads,
	count(distinct(case when call_time is not null then lead_id else null end))::numeric call_leads,
	count(distinct(case when mql_time is not null then lead_id else null end))::numeric mql_leads,
	count(distinct(case when mql_time is not null and lead_id_pr is not null then lead_id else null end))::numeric mql_and_prospect_leads,
	count(distinct(case when mql_time is not null and lead_id_pr is not null and mail_time is not null then lead_id else null end))::numeric mql_mail_prospect_leads,
	count(distinct(case when sql_time is not null then lead_id else null end))::numeric sql_leads,
	count(distinct(case when sql_time is not null and lead_id_pr is not null then lead_id else null end))::numeric sql_and_prospect_leads,
	count(distinct(case when sql_time is not null and lead_id_pr is not null and mail_time is not null then lead_id else null end))::numeric sql_mail_prospect_leads,
	count(distinct(case when sal_time is not null then lead_id else null end))::numeric sal_leads,
	count(distinct(case when sal_time is not null and lead_id_pr is not null then lead_id else null end))::numeric sal_and_prospect_leads,
	count(distinct(case when sal_time is not null and lead_id_pr is not null and mail_time is not null then lead_id else null end))::numeric sal_mail_prospect_leads,
	count(distinct(case when aql_time is not null then lead_id else null end))::numeric aql_leads,
	count(distinct(case when aql_time is not null and lead_id_pr is not null then lead_id else null end))::numeric aql_and_prospect_leads,
	count(distinct(case when aql_time is not null and lead_id_pr is not null and mail_time is not null then lead_id else null end))::numeric aql_mail_prospect_leads,
	count(distinct(case when sdr_time is not null then lead_id else null end))::numeric sdr_leads,
	count(distinct(case when sdr_time is not null and lead_id_pr is not null then lead_id else null end))::numeric sdr_and_prospect_leads,
	count(distinct(case when sdr_time is not null and lead_id_pr is not null and mail_time is not null then lead_id else null end))::numeric sdr_mail_prospect_leads	
from (
select a.*, b.*
from tat_data a
left join task_data b on a.lead_id = b.lead_id_pr
) a 
