with mail_tp as (
select
	dc.id as opportunity_id, m.delivered_at as timestamp, 'mailing' as activity, m.subject, m.body_text
from
	claroty_outreach.mailing m
join claroty_outreach.data_connection dc on dc.parent_id = m.relationship_opportunity_id
where
	1 = 1
	and m.relationship_opportunity_id is not null
	and dc.type = 'Opportunity'
)
, call_tp as (
select
	dc.id as opportunity_id, c.completed_at as timestamp, 'calling' as activity, null as subject, null as body_text
from
	claroty_outreach."call" c
join claroty_outreach.data_connection dc on dc.parent_id = c.relationship_opportunity_id
where
	1 = 1
	and c.relationship_opportunity_id is not null
	and dc.type = 'Opportunity'
)
, history_data as (
select
	opportunity_id, created_date as timestamp,	stage_name as activity,	null as subject, null as body_text
from
	claroty_salesforce.opportunity_history oh 
)
, account_history_data as (
select o.id, ah.created_date as timestamp , ah.new_value as activity, null as subject, null as body_text 
from claroty_salesforce.opportunity o 
left join claroty_salesforce.account_history ah on o.account_id = ah.account_id 
where 1 =1 
	-- ah.account_id = '0015800001SYHZJAA5' 
	and field ilike '%status%'
order by 1, 3 
)
, opp_created_data as (
select
	id as opportunity_id,	created_date as timestamp,	'Opportunity Created' as activity,	null as subject,	null as body_text
from
	claroty_salesforce.opportunity 
)
, opp_events as ( 
select what_id , e.activity_date_time , concat('event_',e."type") as activity , e.subject ,e.description as body_text
from claroty_salesforce."event" e 
where what_id like '%006%' 
)
, tp_data as (
select	* from	history_data
union all
select	* from	mail_tp
union all
select	* from	call_tp
union all
select	* from	opp_created_data
union all 
select	* from	opp_events
union all
select * from account_history_data
)
-- final_data as (
select
	opportunity_id,	timestamp,	activity,	subject,	body_text
from
	(
	select
		*, case	when activity = 'Opportunity Created' then 0	else 1 end as sort_idx
	from	tp_data
	) g
order by
	opportunity_id,	timestamp,	sort_idx
